# Recipes App(Edamam)

* **Platform:** iOS (Objective-C)
* **Aplication Language:** English
* **Code Language:** English

## Description
A simple Objective-C app that consumes Edamam API using MVC architecture with separated networking.

The app desn't use any POD's or third party code.



## Main Functionalities
* Recipes lists 
* Recipe description
* Recipes search
* Async API call
* API response await with dispatch group
* Image caching with NSCache
* Assync image loading

