//
//  RecipesModel.m
//  ObjectiveCProject
//
//  Created by Nourhan Hamada on 16/06/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//
#import "RecipesModel.h"

@implementation RecipesModel

-(id) initWithDictionary:(NSDictionary *)dictionary  {
    
    self = [self init];
    
    if (self) {
        NSDictionary *recip = [dictionary valueForKeyPath:@"recipe"];
        NSArray *healtyLabel = [recip valueForKeyPath:@"healthLabels"];
        NSArray *ingredientsLabel = [recip valueForKeyPath:@"ingredientLines"];

        NSString *titleRecipe = recip[@"label"];
        NSString *source = recip[@"source"];
        NSString *imageUrl = recip[@"image"];
        NSString *preferenceUrl = recip[@"url"];

        self.recipeObjc = recip;
        self.titleRecipe = titleRecipe;
        self.sourceRecipe = source;
        self.healtyLabelsRecipe = healtyLabel;
        self.ingredientLines = ingredientsLabel;
        self.imageUrl = imageUrl;
        self.PreferenceUrl = preferenceUrl;

    }
    
    return self;
}

@end
