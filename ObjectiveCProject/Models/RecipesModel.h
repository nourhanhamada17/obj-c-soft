//
//  RecipesModel.h
//  ObjectiveCProject
//
///  Created by Nourhan Hamada on 16/06/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecipesModel : NSObject
@property (strong, nonatomic) NSDictionary *recipeObjc;
@property (strong, nonatomic) NSArray *healtyLabelsRecipe;
@property (strong, nonatomic) NSArray *ingredientLines;

@property (strong, nonatomic) NSString *titleRecipe;
@property (strong, nonatomic) NSString *sourceRecipe;
@property (strong, nonatomic) NSString *imageUrl;
@property (strong, nonatomic) NSString *PreferenceUrl;

-(id) initWithDictionary:(NSDictionary *) dictionary;

@end




