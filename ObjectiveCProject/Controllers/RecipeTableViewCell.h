//
//  RecipeTableViewCell.h
//  ObjectiveCProject
//
//  Created by Nourhan Hamada on 16/06/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecipesModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface RecipeTableViewCell : UITableViewCell

//MARK: - Constants
+ (NSString *) identifier;

//MARK: - Outlets
@property (weak, nonatomic) IBOutlet UILabel *recipeTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *recipeSourceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *recipeImageView;
@property (weak, nonatomic) IBOutlet UILabel *recipeHealtyLabels;

//MARK: - Methods
-(void) configureWithRecipe:(RecipesModel *) recipe;

@end

NS_ASSUME_NONNULL_END
