//
//  ViewController.h
//  ObjectiveCProject
//
//  Created by Nourhan Hamada on 16/06/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UITableViewController
<UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@end

