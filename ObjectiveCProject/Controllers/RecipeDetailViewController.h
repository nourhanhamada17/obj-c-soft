//
//  RecipeDetailViewController.h
//  ObjectiveCProject
//
//  Created by Nourhan Hamada on 16/06/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecipesModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface RecipeDetailViewController : UIViewController

// MARK: - Outlets
@property (weak, nonatomic) IBOutlet UIImageView *recipeCoverImageView;
@property (weak, nonatomic) IBOutlet UILabel *recipeTitleLabel;
@property (weak, nonatomic) IBOutlet UITextView *recipeIngredientsTV;
@property (weak, nonatomic) IBOutlet UILabel *recipePublisherUrlLabel;

// MARK: Variables
@property (nonatomic) RecipesModel *recipe;


@end

NS_ASSUME_NONNULL_END
