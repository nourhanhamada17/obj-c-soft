//
//  RecipeDetailViewController.m
//  ObjectiveCProject
//
//  Created by Nourhan Hamada on 16/06/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//

#import "RecipeDetailViewController.h"
#import "RecipesModel.h"
#import "RecipesRequest.h"
#import <SafariServices/SafariServices.h>

@interface RecipeDetailViewController ()

@end
@interface RecipeDetailViewController () <SFSafariViewControllerDelegate>

@end


@implementation RecipeDetailViewController


- (void)displaySafari {
    SFSafariViewController *safariVC = [[SFSafariViewController alloc]initWithURL:[NSURL URLWithString:  self.recipe.PreferenceUrl ] entersReaderIfAvailable:NO];
    safariVC.delegate = self;
    [self presentViewController:safariVC animated:NO completion:nil];
}

#pragma mark - SFSafariViewController delegate methods
-(void)safariViewController:(SFSafariViewController *)controller didCompleteInitialLoad:(BOOL)didLoadSuccessfully {
  // Load finished
}

-(void)safariViewControllerDidFinish:(SFSafariViewController *)controller {
  // Done button pressed
}

// MARK: - Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Recipe Detail";
    [self configureWithRecipe:self.recipe];
}


// MARK: - Private Methods
- (void)configureWithRecipe:(RecipesModel *)recipe {
    
    /// Sets texts
    self.recipeTitleLabel.text = recipe.titleRecipe;
    
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString: recipe.PreferenceUrl];
    [attributeString addAttribute:NSUnderlineStyleAttributeName
                            value:[NSNumber numberWithInt:1]
                            range:(NSRange){0,[attributeString length]}];
    self.recipePublisherUrlLabel.attributedText = attributeString;

    
    UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedOnLink:)];
    // if labelView is not set userInteractionEnabled, you must do so
    [_recipePublisherUrlLabel setUserInteractionEnabled:YES];
    [_recipePublisherUrlLabel addGestureRecognizer:gesture];
    
    self.recipeIngredientsTV.text = [recipe.ingredientLines componentsJoinedByString:@"\n"];
    /// Layout
    self.recipeCoverImageView.layer.cornerRadius = 16;
    [self.recipeCoverImageView.layer setBorderColor: [[UIColor grayColor] CGColor]];
    [self.recipeCoverImageView.layer setBorderWidth: 1.0];
    
    self.recipeIngredientsTV.sizeToFit;
    self.recipeIngredientsTV.scrollEnabled = false;
    
    /// Loads recipe cover
    NSLog(@"Will load cover for: %@", recipe.imageUrl);
    
    if (recipe.imageUrl == (NSString *)[NSNull null]) {
        NSLog(@"No cover image available");
        return;
    }
    
    NSLog(@"Loading cover from: %@", recipe.imageUrl);
    [RecipesRequest getRecipeImageDataFromPath:recipe.imageUrl  andSize:medium andHandler:^(NSData *data)  {

        if ( data == nil ) {
            NSLog(@"Image data response was NULL");
            self.recipeCoverImageView.image = nil;
            return;
        }
        
        UIImage *image = [UIImage imageWithData:data];
        
        if (image == nil) {
            NSLog(@"Error converting data response to image");
            self.recipeCoverImageView.image = nil;
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.recipeCoverImageView.image = image;
            NSLog(@"Successfuly loaded image");
            
        });
        
    }];
    
}

-(void) userTappedOnLink:(UITapGestureRecognizer *) sender
{
    [self displaySafari];
  NSLog(@"Tap on publisher ");
}

@end


