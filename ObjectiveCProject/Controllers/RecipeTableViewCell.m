//
//  RecipeTableViewCell.m
//  ObjectiveCProject
//
//  Created by Nourhan Hamada on 16/06/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//

#import "RecipeTableViewCell.h"
#import "RecipesRequest.h"

@interface RecipeTableViewCell()

@property (strong, nonatomic) NSURLSessionTask *coverSessionTask;

@end

@implementation RecipeTableViewCell

// MARK: - Constants
+ (NSString *)identifier { return @"RecipeTableViewCell"; }

// MARK: - Lifecycle
- (void)awakeFromNib {
    [super awakeFromNib];
}

// MARK: - Config
- (void)configureWithRecipe:(RecipesModel *)recipe {
    
    /// Sets texts
    self.recipeTitleLabel.text = recipe.titleRecipe;
    self.recipeSourceLabel.text = recipe.sourceRecipe;
    self.recipeHealtyLabels.text = [recipe.healtyLabelsRecipe componentsJoinedByString:@" - "];
    self.recipeImageView.layer.cornerRadius = 8;
        
    [self.recipeImageView.layer setBorderColor: [[UIColor grayColor] CGColor]];
    [self.recipeImageView.layer setBorderWidth: 1.0];
    
    /// Cancels current cover loading
    /*
     If there is a image request in course,
     it is canceled. This way, images wont "blink",
     and network use will be lower
    */
    [self.coverSessionTask cancel];
    
    /// Loads recipe cover
    NSLog(@"Will load cover for: %@", recipe.imageUrl);
    
    if (recipe.imageUrl == (NSString *)[NSNull null]) {
        NSLog(@"No cover image available");
        self.recipeImageView.image = nil;
        return;
    }
    
    NSLog(@"Loading cover from: %@", recipe.imageUrl);
    self.coverSessionTask = [RecipesRequest getRecipeImageDataFromPath:recipe.imageUrl  andSize:small andHandler:^(NSData *data)  {

        if ( data == nil ) {
            NSLog(@"Image data response was NULL");
            self.recipeImageView.image = nil;
            return;
        }
        UIImage *image = [UIImage imageWithData:data];
        
        if (image == nil) {
            NSLog(@"Error converting data response to image");
            self.recipeImageView.image = nil;
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.recipeImageView.image = image;
            NSLog(@"Successfuly loaded image");
            
        });
        
    }];
}

@end
