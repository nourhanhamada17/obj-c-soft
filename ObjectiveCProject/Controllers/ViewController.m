//
//  ViewController.m
//  ObjectiveCProject
//
//  Created by Nourhan Hamada on 16/06/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//

#import "ViewController.h"
#import "RecipesModel.h"
#import "RecipesRequest.h"
#import "RecipeTableViewCell.h"
#import "RecipeDetailViewController.h"
#import "Reachability.h"

@interface ViewController ()
//MARK: - Variables
@property (strong, nonatomic) NSMutableArray<RecipesModel *> *searchedRecipes;
@property (strong, nonatomic) RecipesModel *selectedRecipe;
@property (strong, nonatomic) UISearchController *searchBarController;
@property (nonatomic) BOOL showldDisplaySearch;
@property (strong, nonatomic) NSURLSessionTask *currentSearchTask;
@end

@implementation ViewController

// MARK: - Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Recipes";
    if (@available(iOS 11.0, *)) {
        self.navigationController.navigationBar.prefersLargeTitles = YES;
    } else {
        // Fallback on earlier versions
    }
    [self setupSearchBar];
    [self loadRecipes];
}


// MARK: - Methods
-(void) setupSearchBar {
    /// Creates searchbar
    self.searchBarController = UISearchController.new;
    
    /// Sets searchbar appearence and behaiviour
    [self.searchBarController.searchBar sizeToFit];
    if (@available(iOS 9.1, *)) {
        self.searchBarController.obscuresBackgroundDuringPresentation = false;
    } else {
        // Fallback on earlier versions
    }
    self.searchBarController.hidesNavigationBarDuringPresentation = true;
    self.searchBarController.searchBar.searchTextField.clearButtonMode = UITextFieldViewModeNever;
    self.searchBarController.searchBar.returnKeyType = UIReturnKeyDone;
    
    /// Sets searchbar delegates
    self.searchBarController.searchBar.delegate = self;
    self.searchBarController.searchBar.searchTextField.delegate = self;
    
    /// Adds searchbar to navigation
    if (@available(iOS 11.0, *)) {
        self.navigationItem.searchController = self.searchBarController;
    } else {
        // Fallback on earlier versions
    }

}

-(void) loadRecipes {
    dispatch_group_t group = dispatch_group_create();
    dispatch_group_wait(group, DISPATCH_TIME_FOREVER);
    dispatch_async(dispatch_get_main_queue(), ^{
        self.showldDisplaySearch = NO;
        [self.tableView reloadData];
    });
}

-(void) searchForRecipesWithQuery: (NSString *) query {
    /*
     In case there is a search in progress,
     it is cancelled, in order to save data
     download and parsing.
     */
    
    [self.currentSearchTask cancel];
    self.searchedRecipes = NSMutableArray.new;
    [self.searchBarController.searchBar setShowsCancelButton: YES animated: YES];
    
    Reachability *reach = [Reachability reachabilityForInternetConnection];
    
    if ([reach isReachable]) {
        NSLog(@"Device is connected to the internet");
        
       UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.center = CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0);
        [self.view addSubview: activityIndicator];

        [activityIndicator startAnimating];
        
        self.currentSearchTask = [RecipesRequest searchRecipesWithQuery:query andHandler:^(NSMutableArray *recipes) {
            [self.searchedRecipes addObjectsFromArray:recipes];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.showldDisplaySearch = YES;
                [self.tableView reloadData];
                [activityIndicator stopAnimating];

            });
        }];
    }
    else {
        NSLog(@"Device is not connected to the internet");
        [self ShowAlert:@"Please check internet connection."];

    }
    
    
}

- (void) ShowAlert:(NSString *)Message {
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                  message:@""
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIView *firstSubview = alert.view.subviews.firstObject;
    UIView *alertContentView = firstSubview.subviews.firstObject;
    for (UIView *subSubView in alertContentView.subviews) {
        subSubView.backgroundColor = [UIColor grayColor];
    }
    NSMutableAttributedString *AS = [[NSMutableAttributedString alloc] initWithString:Message];
    [AS addAttribute: NSForegroundColorAttributeName value: [UIColor whiteColor] range: NSMakeRange(0,AS.length)];
    [alert setValue:AS forKey:@"attributedTitle"];
    [self presentViewController:alert animated:YES completion:nil];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:^{
        }];
    });
}

- (void)cancelSearch {
    
    [self.currentSearchTask cancel];
    self.searchedRecipes = NSMutableArray.new;
    dispatch_async(dispatch_get_main_queue(), ^{
        self.showldDisplaySearch = NO;
        [self.tableView reloadData];
    });
    
    return;
}

// MARK: - Table View Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.showldDisplaySearch)
        return 1;
    else
        return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.showldDisplaySearch) {
        
        tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        tableView.backgroundView = nil;
        return self.searchedRecipes.count;
    } else {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        noDataLabel.text             = @"No data available";
        noDataLabel.textColor        = [UIColor greenColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        tableView.backgroundView = noDataLabel;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        return 0;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 148;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    if (self.showldDisplaySearch) {
        return @"Recipes";

    } else {
        return @"";
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    RecipeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: [RecipeTableViewCell identifier] forIndexPath:indexPath];
    
    NSMutableArray *recipesArray;
    if (self.showldDisplaySearch) {
        recipesArray = self.searchedRecipes;
    } else {
        
    }
    RecipesModel *recipes = recipesArray[indexPath.row];
    [cell configureWithRecipe:recipes];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSMutableArray *recipesArray;
    if (self.showldDisplaySearch) {
        recipesArray = self.searchedRecipes;
    } else {
    }
    RecipesModel *selectedRecipe = recipesArray[indexPath.row];
    self.selectedRecipe = selectedRecipe;
    [self performSegueWithIdentifier:@"detail" sender:nil];
}


// MARK: - Prepare for Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    RecipeDetailViewController *ricpeDetailVC = [segue destinationViewController];
    ricpeDetailVC.recipe = self.selectedRecipe;
}


// MARK: - Text Field Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
   
    [textField resignFirstResponder];
    return YES;
}


// MARK: - Search Bar Delegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [self.searchBarController.searchBar setShowsCancelButton: YES animated: YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [self.searchBarController.searchBar setShowsCancelButton:NO animated:YES];
    self.searchBarController.searchBar.text = @"";
    [self cancelSearch];
    [searchBar resignFirstResponder];
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    if (searchText.length == 0) {
        [self cancelSearch];
        return;
    }
    
    [self searchForRecipesWithQuery:searchText];
}

@end
