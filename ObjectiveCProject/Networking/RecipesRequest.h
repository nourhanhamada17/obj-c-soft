//
//  RecipeDBRequest.h
//  ObjectiveCProject
//
//  Created by Nourhan Hamada on 16/06/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//

#ifndef RecipeDBRequest_h
#define RecipeDBRequest_h

#import "RecipesModel.h"

#endif /* RecipeDBRequest_h */

typedef enum {
    small,
    medium,
    large
} ImageSize;

@interface RecipesRequest: NSObject

+ (NSURLSessionTask *) getRecipeImageDataFromPath:(NSString *)recipeImagePath andSize:(ImageSize) imageSize andHandler:(void (^)(NSData *))handler;
+ (NSURLSessionTask *) searchRecipesWithQuery:(NSString *)query andHandler:(void (^)(NSMutableArray *))handler;

@end
