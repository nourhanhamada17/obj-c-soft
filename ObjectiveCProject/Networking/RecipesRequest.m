//
//  RecipeDBRequest.m
//  ObjectiveCProject
//
//  Created by Nourhan Hamada on 16/06/20.
//  Copyright © 2020 Nourhan Hamada. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RecipesRequest.h"

@interface RecipesRequest()


@end

static NSCache *imageCache;

@implementation RecipesRequest


//pathURl
NSString *searchUrl = @"https://api.edamam.com/search?app_key=b0c4f8afaa13f79ee3219775e0d90d23&app_id=89193820";

//cashing image
+ (NSCache *) imageCache {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        imageCache = NSCache.new;
    });
    return imageCache;
}

+(NSString *) searchURLWithQuery: (NSString *)query {
    
    return [NSString stringWithFormat:@"%@&q=%@", searchUrl, query];
}



+ (NSData *) getCacheImageWithUrl: (NSString *) url {
    
    NSData *imageData = [[self imageCache] objectForKey:url];
    
    return imageData;
}

+ (void) cacheImageData: (NSData *) data atUrl: (NSString *) url {
    
    [[self imageCache] setObject:data forKey:url];
}

+ (NSURLSessionTask *) getRecipeImageDataFromPath:(NSString *)recipeImagePath andSize:(ImageSize)size andHandler:(void (^)(NSData *))handler {
    
    
    NSString *imageFullPath = recipeImagePath;
    
    /// Checks cache for image data
    NSData *cachedImageData = [self getCacheImageWithUrl:imageFullPath];
    if ( cachedImageData != nil ) {
        
        NSLog(@"Did found image on cache.");
        
        handler(cachedImageData);
        return nil;
    }
    
    
    NSURL *url = [NSURL URLWithString:imageFullPath];
    
    NSURLSessionTask *session = [NSURLSession.sharedSession dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if(error) {
            NSLog(@"Failed to geet image data: %@", error);
            handler(nil);
            return;
        }
        
        [self cacheImageData:data atUrl:imageFullPath];
        handler(data);
        
    }];
    
    [session resume]; /// Sends the request
    return session;
}

+ (NSURLSessionTask *) searchRecipesWithQuery:(NSString *)query andHandler:(void (^)(NSMutableArray *))handler {
    
    NSLog(@"Searching for recipes with query: %@", query);
    
    /// Encode query
    NSString *charactersToEscape = @"!*'();:@&=+$,/?%#[] ";
    NSCharacterSet *allowedCharacters = [[NSCharacterSet characterSetWithCharactersInString:charactersToEscape] invertedSet];
    NSString *encodedQuery = [query stringByAddingPercentEncodingWithAllowedCharacters:allowedCharacters];
    
    NSURL *url = [NSURL URLWithString: [self searchURLWithQuery:encodedQuery]];
    
    NSLog(@"Sending request: %@", url);
    
    
  
    
    NSURLSessionTask *task = [NSURLSession.sharedSession dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data == nil) return;
        NSLog(@"Did receive API data.");
        
        /// Uncomment these lines to se
        /// the returned recipes list printed
        /// on console
        /*
         NSString *jsonResponse = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
         NSLog(@"Response: %@", jsonResponse);
         */
        
        NSError *err;
        NSDictionary *resultJSON = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&err];
        if (err) {
            NSLog(@"Failed to serialize data into JSON: %@", err);
            handler(NSMutableArray.new);
            return;
        }
        
        NSArray *recipesDictionaryArray = resultJSON[@"hits"];
        NSLog(@"Did serialize data into JSON.");
        
        NSMutableArray *recipes = NSMutableArray.new;
        for (NSDictionary *recipeDictionary in recipesDictionaryArray) {
            RecipesModel *recipe = RecipesModel.new;
            recipe = [recipe initWithDictionary:recipeDictionary];
            [recipes addObject:recipe];
        }
        
        NSLog(@"Did fetch json into recipes array.");
        handler(recipes);
        
        
    }];
    
    [task resume];
    return task;
}


@end

